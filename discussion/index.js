// 1. Use a require directive to acquire express library and its utilities
const express = require('express');
// 2. Create a server using only express
const server = express();
// express() -> will allow creation of express application

// 3. Identify a port/address that will serve/host the newly created connection/app
const address = 3000;
// 4. Bind the app to the desired designated port using the listen(), create a method that will display
server.listen(address, () => {
	console.log(`Server is running on port ${address}`);
});
